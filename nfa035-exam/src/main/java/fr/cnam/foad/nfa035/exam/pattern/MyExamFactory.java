package fr.cnam.foad.nfa035.exam.pattern;

import java.util.Date;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import fr.cnam.foad.nfa035.exam.model.MyWhiteExam;

@Component
public class MyExamFactory
{

@Bean    
public MyWhiteExam getCurrentExam(){
        return new MyWhiteExam("Bibliotheques et Patterns", "NFA035", "CNAM", "Ile de France", new Date());
        
    }

}
