package fr.cnam.foad.nfa035.exam.model;


import java.util.Date;


public class MyWhiteExam {

    private String intitule;
    private String codeUe;
    private String ecole;
    private String region;
    private Date date;

    
    public MyWhiteExam(String intitule, String codeUe, String ecole, String region, Date date) {
        this.intitule = intitule;
        this.codeUe = codeUe;
        this.ecole = ecole;
        this.region = region;
        this.date = date;
    }

    public String getIntitule() {
        return intitule;
    }



    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }



    public String getCodeUe() {
        return codeUe;
    }



    public void setCodeUe(String codeUe) {
        this.codeUe = codeUe;
    }

    public String getEcole() {
        return ecole;
    }

    public void setEcole(String ecole) {
        this.ecole = ecole;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "MyExam{" +
                "intitule='" + intitule + '\'' +
                ", codeUe='" + codeUe + '\'' +
                ", ecole='" + ecole + '\'' +
                ", region='" + region + '\'' +
                ", date=" + date.getTime() +
                '}';
    }

}
